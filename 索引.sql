select * from testIndex
-- 显示加入的全部索引
show index from testIndex
-- 创建索引
create index nameIdex on testIndex(name)
create index ageIdex on testIndex(age)
-- 另一种方式加索引
alter table testIndex add column card bigint
-- 删除索引
drop  index card on testIndex
-- 添加唯一索引
alter table testIndex add UNIQUE(card)
explain select * from testIndex where name like '测试%'
explain select * from testIndex where name like '%测试%' and card is null
-- 创建存储过程加大量数据
create PROCEDURE addData1()
begin
declare i int;
set i=100;
while i<10000000
do
insert into testIndex(name,age,card) VALUES (concat('测试',i),i,1234567894561235);
 set i=i+1;
end while ;
end;


call addData1()