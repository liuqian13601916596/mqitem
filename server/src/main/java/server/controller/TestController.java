package server.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/9/28.
 */
@RestController
@RefreshScope
public class TestController {

    @Value("${name}")
    String name;

    @GetMapping("/provider/test")
    public String test() {
        System.out.println("获取的" + name);
        return "我是提供者";
    }
}
