package app.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * 滚动查询结构
 * @param <T>
 */
@Data
public class BaseRollPageResult<T> implements Serializable {
    private boolean hasNext;
    private Long next;
    private Integer size;
    private Collection<T> records = Collections.emptyList();
}
