package app.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 分页查询结构
 * @param <T>
 */
@Data
public class BasePageResult<T> implements Serializable {
    private Long size;
    private Long page;
    private Long total;
    private Long totalPage;
    protected Collection<T> records = Collections.emptyList();

    public BasePageResult() {
    }

    public BasePageResult(Long page, Long size, Long total, List<T> records) {
        this.page = page;
        this.size = size;
        this.total = total;
        this.records = records;
    }

    public boolean hasNext() {
        return this.page < this.totalPage;
    }

    public Long getTotalPage() {
        if (this.size != null && this.size != 0L && this.total != null && this.total != 0L) {
            this.totalPage = this.total / this.size;
            if (this.total % this.size > 0L) {
                Long var2 = this.totalPage;
                Long var3 = this.totalPage = this.totalPage + 1L;
            }

            return this.totalPage;
        } else {
            return 0L;
        }
    }

    public Long getSize() {
        return this.size;
    }

    public Long getPage() {
        return this.page;
    }

    public Long getTotal() {
        return this.total;
    }

    public Collection<T> getRecords() {
        return this.records;
    }

    public void setSize(final Long size) {
        this.size = size;
    }

    public void setPage(final Long page) {
        this.page = page;
    }

    public void setTotal(final Long total) {
        this.total = total;
    }

    public void setTotalPage(final Long totalPage) {
        this.totalPage = totalPage;
    }

    public void setRecords(final Collection<T> records) {
        this.records = records;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof BasePageResult)) {
            return false;
        } else {
            BasePageResult<?> other = (BasePageResult)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$size = this.getSize();
                    Object other$size = other.getSize();
                    if (this$size == null) {
                        if (other$size == null) {
                            break label71;
                        }
                    } else if (this$size.equals(other$size)) {
                        break label71;
                    }

                    return false;
                }

                Object this$page = this.getPage();
                Object other$page = other.getPage();
                if (this$page == null) {
                    if (other$page != null) {
                        return false;
                    }
                } else if (!this$page.equals(other$page)) {
                    return false;
                }

                label57: {
                    Object this$total = this.getTotal();
                    Object other$total = other.getTotal();
                    if (this$total == null) {
                        if (other$total == null) {
                            break label57;
                        }
                    } else if (this$total.equals(other$total)) {
                        break label57;
                    }

                    return false;
                }

                Object this$totalPage = this.getTotalPage();
                Object other$totalPage = other.getTotalPage();
                if (this$totalPage == null) {
                    if (other$totalPage != null) {
                        return false;
                    }
                } else if (!this$totalPage.equals(other$totalPage)) {
                    return false;
                }

                Object this$records = this.getRecords();
                Object other$records = other.getRecords();
                if (this$records == null) {
                    if (other$records == null) {
                        return true;
                    }
                } else if (this$records.equals(other$records)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BasePageResult;
    }

    public int hashCode() {
        int result = 1;
        Object $size = this.getSize();
         result = result * 59 + ($size == null ? 43 : $size.hashCode());
        Object $page = this.getPage();
        result = result * 59 + ($page == null ? 43 : $page.hashCode());
        Object $total = this.getTotal();
        result = result * 59 + ($total == null ? 43 : $total.hashCode());
        Object $totalPage = this.getTotalPage();
        result = result * 59 + ($totalPage == null ? 43 : $totalPage.hashCode());
        Object $records = this.getRecords();
        result = result * 59 + ($records == null ? 43 : $records.hashCode());
        return result;
    }

    public String toString() {
        return "BasePageResult(size=" + this.getSize() + ", page=" + this.getPage() + ", total=" + this.getTotal() + ", totalPage=" + this.getTotalPage() + ", records=" + this.getRecords() + ")";
    }
}
