package app.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 组件菜单，树结构
 */
@Data
public class MenuResponse implements Serializable {
    private Long id;
    private String name;
    /**
     * 父节点，根节点为0
     */
    private Long parentId;
    /**
     * 子节点信息
     */
    private List<MenuResponse> childList;

    public MenuResponse(Long id, String name, Long parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }
}
