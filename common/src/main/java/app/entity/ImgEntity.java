package app.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/11/23.
 */
@Data
@ToString
public class ImgEntity implements Serializable {
    private int id;
    private String[] urls;
}
