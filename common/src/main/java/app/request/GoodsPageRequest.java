package app.request;

import lombok.Data;

import java.io.Serializable;
@Data
public class GoodsPageRequest implements Serializable {
    private Long page=1L;
    public Long size=10L;
}
