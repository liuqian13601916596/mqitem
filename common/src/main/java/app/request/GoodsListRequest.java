package app.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2022/1/20.
 */
@Data
public class GoodsListRequest implements Serializable {
    private List<String> idList;
}
