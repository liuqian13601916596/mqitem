package app.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:订单请求
 * @Author: liujinsheng
 * @Date: 2022/1/10.
 */
@Data
public class OrderRequest implements Serializable {
    private String userId;
    private String goodsId;
    private Integer payNum;
}
