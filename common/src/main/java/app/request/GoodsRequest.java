package app.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2022/1/10.
 */
@Data
public class GoodsRequest implements Serializable {
    /**
     * 用户id
     */
    private String userId;
    private String goodsId;
}
