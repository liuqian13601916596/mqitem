package app.request;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class BaseRollPageRequest implements Serializable {
    private Long next=1L;
    private Long nextId;
    private int size = 10;
    private List<OrderItem> orders = new ArrayList();

    public BaseRollPageRequest() {
    }

    public Long getNext() {
        return this.next;
    }

    public Long getNextId() {
        return this.nextId;
    }

    public int getSize() {
        return this.size;
    }

    public List<OrderItem> getOrders() {
        return this.orders;
    }

    public void setNext(final Long next) {
        this.next = next;
    }

    public void setNextId(final Long nextId) {
        this.nextId = nextId;
    }

    public void setSize(final int size) {
        this.size = size;
    }

    public void setOrders(final List<OrderItem> orders) {
        this.orders = orders;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof BaseRollPageRequest)) {
            return false;
        } else {
            BaseRollPageRequest other = (BaseRollPageRequest)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$next = this.getNext();
                Object other$next = other.getNext();
                if (this$next == null) {
                    if (other$next != null) {
                        return false;
                    }
                } else if (!this$next.equals(other$next)) {
                    return false;
                }

                Object this$nextId = this.getNextId();
                Object other$nextId = other.getNextId();
                if (this$nextId == null) {
                    if (other$nextId != null) {
                        return false;
                    }
                } else if (!this$nextId.equals(other$nextId)) {
                    return false;
                }

                if (this.getSize() != other.getSize()) {
                    return false;
                } else {
                    Object this$orders = this.getOrders();
                    Object other$orders = other.getOrders();
                    if (this$orders == null) {
                        if (other$orders != null) {
                            return false;
                        }
                    } else if (!this$orders.equals(other$orders)) {
                        return false;
                    }

                    return true;
                }
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BaseRollPageRequest;
    }

    public int hashCode() {
        int result = 1;
        Object $next = this.getNext();
         result = result * 59 + ($next == null ? 43 : $next.hashCode());
        Object $nextId = this.getNextId();
        result = result * 59 + ($nextId == null ? 43 : $nextId.hashCode());
        result = result * 59 + this.getSize();
        Object $orders = this.getOrders();
        result = result * 59 + ($orders == null ? 43 : $orders.hashCode());
        return result;
    }

    public String toString() {
        return "BaseRollPageRequest(next=" + this.getNext() + ", nextId=" + this.getNextId() + ", size=" + this.getSize() + ", orders=" + this.getOrders() + ")";
    }
}
