package app.request;

public class OrderItem {
    private String column;
    private boolean asc = true;

    public OrderItem() {
    }

    public String getColumn() {
        return this.column;
    }

    public boolean isAsc() {
        return this.asc;
    }

    public void setColumn(final String column) {
        this.column = column;
    }

    public void setAsc(final boolean asc) {
        this.asc = asc;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof OrderItem)) {
            return false;
        } else {
            OrderItem other = (OrderItem)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$column = this.getColumn();
                Object other$column = other.getColumn();
                if (this$column == null) {
                    if (other$column == null) {
                        return this.isAsc() == other.isAsc();
                    }
                } else if (this$column.equals(other$column)) {
                    return this.isAsc() == other.isAsc();
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof OrderItem;
    }

    public int hashCode() {

        int result = 1;
        Object $column = this.getColumn();
         result = result * 59 + ($column == null ? 43 : $column.hashCode());
        result = result * 59 + (this.isAsc() ? 79 : 97);
        return result;
    }

    public String toString() {
        return "OrderItem(column=" + this.getColumn() + ", asc=" + this.isAsc() + ")";
    }
}
