package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/11/16.
 */
@SpringBootApplication
public class CommonApplication {
  public static void main(String[] args) {
      SpringApplication.run(CommonApplication.class,args);
  }
}
