package app.utils.util;

import java.util.Map;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/4/9.
 */
public class MapByValueGetKey {
    /**
     * 根据map的value获取map的key
     * 注意：value相同的值有很多个，都会对应到第一个找到的key上，因此要把找到的key标记，下次不再用
     */
    public String getKey(Map<String,Object> map, String value) {
        String key = "";


        for (Map.Entry<String, Object> entry : map.entrySet()) {

            if (entry.getValue().equals("Reference")) {
                key = entry.getKey();
            }
        }
        return key;
    }
}
