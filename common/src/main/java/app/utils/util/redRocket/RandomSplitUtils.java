package app.utils.util.redRocket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 红包拆包
 */
public class RandomSplitUtils {

    private static BigDecimal split(SplitConfig config) {
        if (config.splitNum == 1) {
            config.splitNum--;
            return config.total.setScale(config.scale, BigDecimal.ROUND_DOWN);
        }

        BigDecimal random = BigDecimal.valueOf(Math.random());
        BigDecimal min   = BigDecimal.valueOf(config.min);

        BigDecimal halfRemainSize = BigDecimal.valueOf(config.splitNum).divide(new BigDecimal(2), BigDecimal.ROUND_UP);
        BigDecimal max1 = config.total.divide(halfRemainSize, BigDecimal.ROUND_DOWN);
        BigDecimal minRemainNum = min.multiply(BigDecimal.valueOf(config.splitNum - 1)).setScale(config.scale, BigDecimal.ROUND_DOWN);
        BigDecimal max2 = config.total.subtract(minRemainNum);
        BigDecimal max = (max1.compareTo(max2) < 0) ? max1 : max2;

        BigDecimal actionTime = random.multiply(max).setScale(config.scale, BigDecimal.ROUND_DOWN);
        actionTime = actionTime.compareTo(min) < 0 ? min: actionTime;

        config.splitNum--;
        config.total = config.total.subtract(actionTime).setScale(config.scale, BigDecimal.ROUND_DOWN);;
        return actionTime;
    }

    /**
     * 随机时间拆分，用于机器人行为
     * @param totalTime
     * @param num
     * @return
     */
    public static List<Long> splitTime(Long totalTime,int num){
        SplitConfig config = new SplitConfig();
        config.total = BigDecimal.valueOf(totalTime);
        config.splitNum = num;
        List<Long> actions = new ArrayList<>();
        while (config.splitNum != 0) {
            BigDecimal result = split(config);
            actions.add(result.longValue());
        }
        return actions;
    }

    /**
     * 金额拆分，用于红包
     * @param total
     * @param num
     * @return
     */
    public static List<BigDecimal> splitMoney(BigDecimal total,int num){
        SplitConfig config = new SplitConfig();
        config.total = total;
        config.splitNum = num;
        config.scale = 2;
        config.min = 0.01;
        List<BigDecimal> actions = new ArrayList<>();
        while (config.splitNum != 0) {
            BigDecimal result = split(config);
            actions.add(result);
        }
        return actions;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            List<BigDecimal> actions = splitMoney(new BigDecimal(100),50);
            System.out.println(actions);
            BigDecimal total = BigDecimal.ZERO;
            for(BigDecimal num:actions){
                total = total.add(num);
            }
            System.out.println("total:"+total);
        }

        for (int i = 0; i < 50; i++) {
      List<Long> actions = splitTime(System.nanoTime(), 50);
            System.out.println(actions);
            Long total = 0L;
            for(Long num:actions){
                total = total+num;
            }
            System.out.println("时间total:"+total);
        }
    }

}
