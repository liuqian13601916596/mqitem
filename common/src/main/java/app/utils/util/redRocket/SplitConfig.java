package app.utils.util.redRocket;

import java.math.BigDecimal;

public class SplitConfig {
    int splitNum; //总共次数
    BigDecimal total; //总数量毫秒
    int scale = 0; //保留小数位
    Double min = 1D; //最小数值
}
