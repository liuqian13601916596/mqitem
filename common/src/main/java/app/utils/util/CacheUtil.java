package app.utils.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wangwenping
 * @description 缓存工具类
 * @date: 2021/3/22.
 */
public class CacheUtil {

    private static final Map<String, Object> CACHE = new ConcurrentHashMap<String, Object>();

    public static void putCache(String key, Object obj, long s) {
        CACHE.put(key, obj);
        String keyTime = new StringBuilder(key).append("KeyTime").toString();
        long expire = System.currentTimeMillis() + (s * 1000);
        CACHE.put(keyTime, expire);
    }

    public static void putCache(String key, Object obj) {
        CACHE.put(key, obj);
    }

    public static Object getCache(String key) {
        String keyTime = new StringBuilder(key).append("KeyTime").toString();
        if (CACHE.containsKey(keyTime)) {

            if (System.currentTimeMillis() > (long) CACHE.get(keyTime)) {
                System.out.println("时间已经过期删除了缓存");
                CACHE.remove(key);
            }
        }
        return CACHE.get(key);
    }

    public static void removeCache(String key) {
        CACHE.remove(key);
        String keyTime = new StringBuilder(key).append("KeyTime").toString();

        CACHE.remove(keyTime);
    }
}
