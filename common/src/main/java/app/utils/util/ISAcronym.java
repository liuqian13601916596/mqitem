package app.utils.util;


/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/3/31.
 */

import java.io.Serializable;

/**
 * 判断字符串大小写
 *
 * @param
 * @return
 */
public class ISAcronym implements Serializable {


    public static boolean isAcronym(String word) {
        //方法一
        boolean bln = false;
        String newstr = word.toUpperCase();
        //是大写
        if (word.equals(newstr)) {
      //要全部为字母
        boolean  isWord=word.matches("[a-zA-Z]+");
        if(isWord){
            bln=true;
            return true;
        }else{
            bln=false;
            return bln;
        }

        }else {
            bln=false;
            return bln;
        }


    }


    public static void main(String[] args) {
        String ss = "Adfswsf";
       System.out.println(   ISAcronym.isAcronym("RESOURCE1"));

        //方法一

        String str = "成功";
        String newstr = str.toUpperCase();
        if (str.equals(newstr)) {
            System.out.println("是大写");
        }else {
            System.out.println("不是");
        }
    }
}
