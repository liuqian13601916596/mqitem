package app.utils.util;


/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/4/6.
 */


import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 判断是否有中文
 */
public class IsChineseUtil implements Serializable {
    /**
     * 字符串是否包含中文
     *
     * @param str 待校验字符串
     * @return true 包含中文字符 false 不包含中文字符
     * @throws
     */
    public static boolean isContainChinese(String str)  {


        Pattern p = Pattern.compile("[\u4E00-\u9FA5|\\！|\\，|\\。|\\（|\\）|\\《|\\》|\\“|\\”|\\？|\\：|\\；|\\【|\\】]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(IsChineseUtil.isContainChinese("记者ddd"));
    }
}
