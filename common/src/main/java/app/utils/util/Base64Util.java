package app.utils.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Util {
    /**
     * 加密
     * @param txt
     * @return
     */
    public static String setBase(String txt){
     return    Base64.getEncoder().encodeToString(txt.getBytes( StandardCharsets.UTF_8 ));

    }

    /**
     * 解密
     * @param txt
     * @return
     */
    public static String getNoBase(String txt){
      return  new String(  Base64.getDecoder().decode(txt),StandardCharsets.UTF_8 );

    }

    public static void main(String[] args) {
        String txt=" finally in Java 855555";
        System.out.println(Base64Util.setBase(txt));
        System.out.println(Base64Util.getNoBase("QmFzZTY0IGZpbmFsbHkgaW4gSmF2YSA4IQ=="));
    }
}
