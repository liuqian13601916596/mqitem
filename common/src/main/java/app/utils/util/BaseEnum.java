//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package app.utils.util;

public interface BaseEnum {
    int getCode();

    String getMsg();

    default Boolean codeEquals(Integer code) {
        return code == null ? Boolean.FALSE : this.getCode() == code;
    }

    default Boolean codeNotEquals(Integer code) {
        return code == null ? Boolean.TRUE : this.getCode() != code;
    }
}
