package app.utils.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @Description:regex对输入特殊字符转义
 * @Author: liujinsheng
 * @Date: 2021/4/24.
 */
public class MongoEscapeSpecialWord {
    //regex对输入特殊字符转义
    public static String escapeSpecialWord(String keyword) {
        if (StringUtils.isNotBlank(keyword)) {
            String[] fbsArr = {"\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|","&"};
            for (String key : fbsArr) {
                if (keyword.contains(key)) {
                    keyword = keyword.replace(key, "\\" + key);
                }

            }

        }
        return keyword;
    }

    public static void main(String[] args) {
        System.out.println(MongoEscapeSpecialWord.escapeSpecialWord("1&2"));
    }
}
