package app.utils.util;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/4/10.
 */
public class Return {
/*
    Map<String, Object> datamap = cmsResourceInfo.getData();
    Map<String, Object> definemap = cmsResourceInfo.getDefine();


                for (String datakey : datamap.keySet()) {
        //keySet获取datamap集合key的集合  然后在遍历key即可
        String datavalue = datamap.get(datakey).toString();
        System.out.println("datakey:" + datakey + " datavalue:" + datavalue);
        //将第一层的data的值变为map集合
        try {
            Map<String, Object> datamap1 = JSON.parseObject(datavalue, Map.class);
            System.out.println("第一层的data集合" + datamap1);
            for (String datakey1 : datamap1.keySet()) {
                String datavalue1 = datamap1.get(datakey1).toString();
                System.out.println("datakey1:" + datakey1 + " datavalue1:" + datavalue1);
                //第二层
                try {
                    Map<String, Object> datamap2 = JSON.parseObject(datavalue1, Map.class);
                    System.out.println("第二层的data集合" + datamap2);
                    for (String datakey2 : datamap2.keySet()) {
                        String datavalue2 = datamap1.get(datakey2).toString();
                        System.out.println("datakey2:" + datakey2 + " datavalue2:" + datavalue2);
                        //第三层
                        try {
                            Map<String, Object> datamap3 = JSON.parseObject(datavalue2, Map.class);
                            System.out.println("第三层的data集合" + datamap3);
                            for (String datakey3 : datamap3.keySet()) {
                                String datavalue3 = datamap3.get(datakey3).toString();
                                System.out.println("datakey3:" + datakey3 + " datavalue3:" + datavalue3);
                                //keySet获取definemap集合key的集合  然后在遍历key即可
                                for (String definekey : definemap.keySet()) {
                                    String definevalue = definemap.get(definekey).toString();
                                    System.out.println("definekey:" + definekey + " definevalue:" + definevalue);
                                    //如果有这个Reference字段，那就将对应data的那个key和值传给前端
                                    if (!StringUtils.isBlank(definevalue)) {
                                        if (definevalue.equals("Reference")) {
                                            System.out.println("第一层的definevalue" + definevalue);
                                            //这个值的key也就是data里面的key,再找到data里面这个key的值，就是资源位，通过这个资源位名来查资源位的data,将这个资源位data赋值给这个key作为值
                                            MapByValueGetKey mapByValueGetKey = new MapByValueGetKey();
                                            String datakeyfinal = mapByValueGetKey.getKey(definemap, definevalue);
                                            //获取的datakey这个值,就是资源位的值
                                            //
                                            System.out.println("获取到第一层最终的key" + datakeyfinal);

                                            System.out.println("当前的map" + datamap1);
                                            String keyToDataValue1 = (String) datamap1.get(datakeyfinal);
                                            //通过值来找对应的资源位
                                            System.out.println("第一层找到的资源位名称" + keyToDataValue1);
                                            CmsResourceInfo cmsResourceInfo1 = null;
                                            if (!StringUtils.isBlank(keyToDataValue1)) {
                                                sourceKey += keyToDataValue1;
                                                String bycomflexvalue = (String) redisTemplate.opsForValue().get(sourceKey);
                                                //存在
                                                if (!StringUtils.isBlank(bycomflexvalue)) {
                                                    cmsResourceInfo1 = JSONObject.parseObject(bycomflexvalue, CmsResourceInfo.class);
                                                } else {
                                                    //不存在
                                                    Criteria criteria = Criteria.where("resourceKey").is(keyToDataValue1);
                                                    Query query = new Query(criteria);
                                                    cmsResourceInfo1 = mongoTemplate.findOne(query, CmsResourceInfo.class);
                                                }
                                                if (cmsResourceInfo1 != null) {
                                                    //获取data
                                                    Map<String, Object> getFromNewKeyMap1 = cmsResourceInfo1.getData();
                                                    //将资源位的key替换成data
                                                    System.out.println("要替换的map" + getFromNewKeyMap1);
                                                    datamap1.put(datakeyfinal, getFromNewKeyMap1);
                                                }
                                            }
                                        }
                                    }
                                    Map<String, Object> sonmap2 = new HashMap<>();
                                    //加上异常处理
                                    if (!StringUtils.isBlank(definevalue)) {
                                        try {
                                            sonmap2 = JSON.parseObject(definevalue, Map.class);
                                            log.info("define子资源位转化过来的map" + sonmap2);
                                            //查找
                                            if (sonmap2.size() > 0) {
                                                for (String sonskey2 : sonmap2.keySet()) {
                                                    String sonstr2 = sonmap2.get(sonskey2).toString();
                                                    System.out.println("sonskey2:" + sonskey2 + " sonstr2:" + sonstr2);
                                                    if (sonstr2.equals("Reference")) {
                                                        System.out.println("第二层的" + sonstr2);
                                                        //这个值的key也就是data里面的key,再找到data里面这个key的值，就是资源位，通过这个资源位名来查资源位的data,将这个资源位data赋值给这个key作为值
                                                        MapByValueGetKey mapByValueGetKey = new MapByValueGetKey();
                                                        String datakeyfinal = mapByValueGetKey.getKey(definemap, sonstr2);
                                                        //获取的datakey这个值,就是资源位的值
                                                        System.out.println("第二层找到的key值" + datakeyfinal);
                                                        String keyToDataValue2 = (String) datamap2.get(datakeyfinal);
                                                        //通过值来找对应的资源位
                                                        System.out.println("当前的map2" + datamap2);
                                                        System.out.println("第二层获取的资源位名称" + keyToDataValue2);
                                                        CmsResourceInfo cmsResourceInfo2 = null;
                                                        if (!StringUtils.isBlank(keyToDataValue2)) {
                                                            sourceKey += keyToDataValue2;
                                                            String bycomflexvalue2 = (String) redisTemplate.opsForValue().get(sourceKey);
                                                            //存在
                                                            if (!StringUtils.isBlank(bycomflexvalue2)) {

                                                                cmsResourceInfo2 = JSONObject.parseObject(bycomflexvalue2, CmsResourceInfo.class);
                                                            } else {
                                                                //不存在
                                                                Criteria criteria = Criteria.where("resourceKey").is(keyToDataValue2);
                                                                Query query = new Query(criteria);
                                                                cmsResourceInfo2 = mongoTemplate.findOne(query, CmsResourceInfo.class);


                                                            }
                                                            if (cmsResourceInfo2 != null) {
                                                                //获取data
                                                                Map<String, Object> getFromNewKeyMap = cmsResourceInfo2.getData();
                                                                //将资源位的key替换成data
                                                                datamap2.put(datakeyfinal, getFromNewKeyMap);
                                                            }
                                                        }
                                                    }
                                                    //第三层
                                                    Map<String, Object> sonmap3 = new HashMap<>();
                                                    //加上异常处理
                                                    if (!StringUtils.isBlank(sonstr2)) {
                                                        try {
                                                            sonmap3 = JSON.parseObject(sonstr2, Map.class);
                                                            log.info("define子资源位转化过来的map3" + sonmap3);
                                                            //查找
                                                            if (sonmap3.size() > 0) {
                                                                for (String sonskey3 : sonmap3.keySet()) {
                                                                    String sonstr3 = sonmap3.get(sonskey3).toString();
                                                                    System.out.println("sonskey3:" + sonskey3 + " sonstr3:" + sonstr3);
                                                                    System.out.println("sonstr3" + sonstr3);
                                                                    if (sonstr3.equals("Reference")) {
                                                                        System.out.println("第三层的" + sonstr3);
                                                                        //这个值的key也就是data里面的key,再找到data里面这个key的值，就是资源位，通过这个资源位名来查资源位的data,将这个资源位data赋值给这个key作为值
                                                                        MapByValueGetKey mapByValueGetKey = new MapByValueGetKey();
                                                                        String datakeyfinal3 = mapByValueGetKey.getKey(sonmap3, sonstr3);
                                                                        //获取的datakey这个值,就是资源位的值
                                                                        System.out.println("这个值对应的key" + datakeyfinal3);
                                                                        //从原始map获取
                                                                        System.out.println("当前的map3" + datamap3);
                                                                        String keyToDataValue3 = (String) datamap3.get(datakeyfinal3);
                                                                        System.out.println("最终获取要的资源位名称" + keyToDataValue3);
                                                                        //通过值来找对应的资源位
                                                                        CmsResourceInfo cmsResourceInfo3 = null;
                                                                        if (!StringUtils.isBlank(keyToDataValue3)) {
                                                                            sourceKey += keyToDataValue3;
                                                                            String bycomflexvalue3 = (String) redisTemplate.opsForValue().get(sourceKey);
                                                                            //存在
                                                                            if (!StringUtils.isBlank(bycomflexvalue3)) {

                                                                                cmsResourceInfo3 = JSONObject.parseObject(bycomflexvalue3, CmsResourceInfo.class);
                                                                            } else {
                                                                                //不存在
                                                                                Criteria criteria = Criteria.where("resourceKey").is(keyToDataValue3);
                                                                                Query query = new Query(criteria);
                                                                                cmsResourceInfo3 = mongoTemplate.findOne(query, CmsResourceInfo.class);


                                                                            }
                                                                            if (cmsResourceInfo3 != null) {
                                                                                //获取data
                                                                                Map<String, Object> getFromNewKeyMap3 = cmsResourceInfo3.getData();
                                                                                //将资源位的key替换成data
                                                                                datamap3.put(datakeyfinal3, getFromNewKeyMap3);
                                                                            }
                                                                        }
                                                                    }
                                                                    //第四层


                                                                }
                                                            }

                                                        } catch (Exception e) {
                                                            sonstr2 = definemap.get(definekey).toString();
                                                            if (sonstr2.equals("Reference")) {
                                                                //这个值的key也就是data里面的key,再找到data里面这个key的值，就是资源位，通过这个资源位名来查资源位的data,将这个资源位data赋值给这个key作为值
                                                                MapByValueGetKey mapByValueGetKey = new MapByValueGetKey();
                                                                String datakeyfinal4 = mapByValueGetKey.getKey(sonmap2, sonstr2);
                                                                //获取的datakey这个值,就是资源位的值
                                                                String keyToDataValue4 = (String) datamap.get(datakeyfinal4);
                                                                //通过值来找对应的资源位
                                                                System.out.println("第三层资源位" + keyToDataValue4);
                                                                CmsResourceInfo cmsResourceInfo4 = null;
                                                                if (!StringUtils.isBlank(keyToDataValue4)) {
                                                                    sourceKey += keyToDataValue4;
                                                                    String bycomflexvalue4 = (String) redisTemplate.opsForValue().get(sourceKey);
                                                                    //存在
                                                                    if (!StringUtils.isBlank(bycomflexvalue4)) {

                                                                        cmsResourceInfo4 = JSONObject.parseObject(bycomflexvalue4, CmsResourceInfo.class);
                                                                    } else {
                                                                        //不存在
                                                                        Criteria criteria = Criteria.where("resourceKey").is(keyToDataValue4);
                                                                        Query query = new Query(criteria);
                                                                        cmsResourceInfo4 = mongoTemplate.findOne(query, CmsResourceInfo.class);
                                                                    }
                                                                    if (cmsResourceInfo4 != null) {
                                                                        //获取data
                                                                        Map<String, Object> getFromNewKeyMap4 = cmsResourceInfo4.getData();
                                                                        //将资源位的key替换成data
                                                                        datamap.put(datakeyfinal4, getFromNewKeyMap4);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }

                                                }
                                            }

                                        } catch (Exception e) {
                                            definevalue = definemap.get(definekey).toString();
                                            if (definevalue.equals("Reference")) {
                                                //这个值的key也就是data里面的key,再找到data里面这个key的值，就是资源位，通过这个资源位名来查资源位的data,将这个资源位data赋值给这个key作为值
                                                MapByValueGetKey mapByValueGetKey = new MapByValueGetKey();
                                                String datakeyfinal = mapByValueGetKey.getKey(definemap, definevalue);
                                                //获取的datakey这个值,就是资源位的值
                                                String keyToDataValue = (String) datamap.get(datakeyfinal);
                                                //通过值来找对应的资源位
                                                System.out.println("没有转化的第一层获取的资源位" + keyToDataValue);
                                                CmsResourceInfo cmsResourceInfo1 = null;
                                                if (!StringUtils.isBlank(keyToDataValue)) {
                                                    sourceKey += keyToDataValue;
                                                    String bycomflexvalue = (String) redisTemplate.opsForValue().get(sourceKey);
                                                    //存在
                                                    if (!StringUtils.isBlank(bycomflexvalue)) {

                                                        cmsResourceInfo1 = JSONObject.parseObject(bycomflexvalue, CmsResourceInfo.class);
                                                    } else {
                                                        //不存在
                                                        Criteria criteria = Criteria.where("resourceKey").is(keyToDataValue);
                                                        Query query = new Query(criteria);
                                                        cmsResourceInfo1 = mongoTemplate.findOne(query, CmsResourceInfo.class);
                                                    }
                                                    if (cmsResourceInfo1 != null) {
                                                        //获取data
                                                        Map<String, Object> getFromNewKeyMap = cmsResourceInfo1.getData();
                                                        //将资源位的key替换成data
                                                        datamap.put(datakeyfinal, getFromNewKeyMap);
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        } catch (Exception exception) {
                            datavalue2 = datamap2.get(datakey2).toString();
                        }
                    }
                } catch (Exception exception) {
                    datavalue1 = datamap1.get(datakey).toString();
                }


            }
        } catch (Exception e) {
            datavalue = datamap.get(datakey).toString();
        }


    }

//                    String toClientSourceKeyStr= (String) redisTemplate.opsForValue().get(toClientSourceKey);
//                       if(!StringUtils.isBlank(toClientSourceKeyStr)){
//                           //转化为map
//                           datamap=JSON.parseObject(toClientSourceKeyStr,Map.class);
//
//                       }else {
//                           if(datamap!=null&&datamap.size()>0){
//                               redisTemplate.opsForValue().set(toClientSourceKey,JSON.toJSONString(datamap));
//                           }
//
//                       }

    //下面这个data就是整合后的data
                if (datamap != null && datamap.size() > 0) {
        System.out.println("最终整合后的data" + datamap);
        cmsResourceBySourceKeyResponse.setData(datamap);
    }*/
}
