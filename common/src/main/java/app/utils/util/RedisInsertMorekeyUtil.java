package app.utils.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.types.Expiration;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @Description:redis批量插入工具
 * @Author: liujinsheng
 * @Date: 2021/5/11.
 */
@Slf4j
@Component
public class RedisInsertMorekeyUtil {
    @Resource
    RedisTemplate redisTemplate;
    //redis批量存数据
    public  void insertMoreKey(List<String> keys, String value){
        log.info("开始批量存储数据");

        //批量get数据
        List<Object> list = redisTemplate.executePipelined(new RedisCallback<String>() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                for (String key : keys) {
                    connection.get(key.getBytes());
                }
                return null;
            }
        });
        //批量set数据
        redisTemplate.executePipelined(new RedisCallback<String>() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                for (int i=0;i<keys.size();i++) {
                    connection.set(keys.get(i).getBytes(),value.getBytes());
                }
                return null;
            }
        });

    }

    /**
     * 功能描述: 使用pipelined批量存储
     *
     * @param: [map, seconds]
     * @return: void
     * @auther: lixk6
     * @date: 2020/4/19 14:34
     */
    public void executePipelined(Map<String, String> map) {
        RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
        redisTemplate.executePipelined(new RedisCallback<String>() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                map.forEach((key, value) -> {
                    connection.set(serializer.serialize(key), serializer.serialize(JSON.toJSONString(value)), Expiration.seconds(600), RedisStringCommands.SetOption.UPSERT);
                });
                return null;
            }
        },serializer);
    }

}
