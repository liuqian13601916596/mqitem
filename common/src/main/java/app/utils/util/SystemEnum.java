//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package app.utils.util;



public enum SystemEnum implements BaseEnum {
    SUCCESS(0, "success"),
    SYSTEM_ERROR(10018, "服务拥挤, 请稍后重试"),
    SENTINEL_LIMIT(10012, "业务太火爆, 请稍后重试"),
    SERVICE_OFFLINE(10011, "服务不可用"),
    NOT_FOUND_ERROR(10016, "访问资源不存在"),
    METHOD_NOT_SUPPORT(10019, "请求不支持"),
    PARAM_ERROR(10020, "参数异常"),
    BUSINESS_ERROR(10022, "业务繁忙, 请稍后重试"),
    SERVICE_ERROR(10018, "服务拥堵, 请稍后重试"),
    SERVICE_TIMEOUT(10021, "服务繁忙, 请稍后重试"),
    SERVICE_LIMIT(10012, "业务太火爆了, 请稍后重试"),
    SERVICE_EXCEPTION(10022, "服务太拥堵, 请稍后重试"),
    NON_LOGIN(10010, "未登录"),
    EXPIRE_ACCESS_TOKEN(10014, "登录过期"),
    EXPIRE_REFRESH_TOKEN(10015, "登录过期"),
    LOCK_MEMBER(10017, "禁用用户"),
    SYSTEM_UPGRADE(10013, "系统升级"),
    ADDRESS_404(10016, "服务地址不存在！");

    private int code;
    private String msg;

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    private SystemEnum(final int code, final String msg) {
        this.code = code;
        this.msg = msg;
    }
}
