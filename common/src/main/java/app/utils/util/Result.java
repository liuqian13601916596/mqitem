//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package app.utils.util;


import java.io.Serializable;

public class Result<R> implements Serializable {
    private boolean success;
    private int code;
    private String msg;
    private R data;
    public static final Integer defErrCode = -1;

    public static <R> Result<R> ofSuccess(R data) {
        return (new Result()).setSuccess(true).setCode(SystemEnum.SUCCESS.getCode()).setMsg(SystemEnum.SUCCESS.getMsg()).setData(data);
    }

    public static <R> Result<R> ofSuccessMsg(String msg) {
        return (new Result()).setSuccess(true).setMsg(msg);
    }

    public static <R> Result<R> ofFailMsg(String msg) {
        return (new Result()).setSuccess(false).setCode(defErrCode).setMsg(msg);
    }

    public static <R> Result<R> ofFail(BaseEnum baseEnum) {
        Result<R> result = new Result();
        result.setSuccess(false);
        result.setCode(baseEnum.getCode());
        result.setMsg(baseEnum.getMsg());
        return result;
    }

    public static <R> Result<R> ofFail(Integer code, String msg) {
        Result<R> result = new Result();
        result.setSuccess(false);
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public Result<R> setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public Result<R> setCode(int code) {
        this.code = code;
        return this;
    }

    public Result<R> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Result<R> setData(R data) {
        this.data = data;
        return this;
    }

    public Result() {
    }

    public boolean isSuccess() {
        return this.success;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public R getData() {
        return this.data;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof Result)) {
            return false;
        } else {
            Result<?> other = (Result)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.isSuccess() != other.isSuccess()) {
                return false;
            } else if (this.getCode() != other.getCode()) {
                return false;
            } else {
                label40: {
                    Object this$msg = this.getMsg();
                    Object other$msg = other.getMsg();
                    if (this$msg == null) {
                        if (other$msg == null) {
                            break label40;
                        }
                    } else if (this$msg.equals(other$msg)) {
                        break label40;
                    }

                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Result;
    }

    @Override
    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
         result = result * 59 + (this.isSuccess() ? 79 : 97);
        result = result * 59 + this.getCode();
        Object $msg = this.getMsg();
        result = result * 59 + ($msg == null ? 43 : $msg.hashCode());
        Object $data = this.getData();
        result = result * 59 + ($data == null ? 43 : $data.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Result(success=" + this.isSuccess() + ", code=" + this.getCode() + ", msg=" + this.getMsg() + ", data=" + this.getData() + ")";
    }
}
