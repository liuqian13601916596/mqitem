package app.utils.util;

/**
 * 用来检验是否符合UUID
 */
public class IsUUidUtil {
    public static boolean isValidUUID(String uuid) {
        // UUID校验
        if (uuid == null) {
            System.out.println("uuid is null");
        }
        String regex = "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$";
        if (uuid.matches(regex)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(IsUUidUtil.isValidUUID("1d49acf7-554e-42eb-a88f-ce5d2a5686e2"));
    }
}
