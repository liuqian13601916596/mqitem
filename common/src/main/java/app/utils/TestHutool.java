package app.utils;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.qrcode.QrCodeException;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import cn.hutool.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * @Description:测试hutool的工具
 * @Author: liujinsheng
 * @Date: 2021/11/16.
 */
@Slf4j
public class TestHutool {
    // 自定义参数，这部分是Hutool工具封装的
    private static QrConfig initQrConfig() {
        QrConfig config = new QrConfig(300, 300);
        // 设置边距，既二维码和背景之间的边距
        config.setMargin(3);
        // 设置前景色，既二维码颜色（青色）
        config.setForeColor(Color.CYAN.getRGB());
        // 设置背景色（灰色）
        config.setBackColor(Color.GRAY.getRGB());
        return config;
    }
    /**
     * 时间转化
     */
     public static void testDate(){
         //时间转化为标准格式
    String time=     DateUtil.format(new Date(),"yyyy-MM-dd hh:mm:ss");
    System.out.println("当前时间"+time);
       String t1=  DateUtil.formatDateTime(new Date());
    System.out.println("正确格式"+t1);
    System.out.println("现在时间"+DateUtil.now());
    String timeStr="2021-11-16 15:01:28";
     Date date=    DateUtil.parse(timeStr,"yyyy-MM-dd hh:mm:ss");
    System.out.println("时间字符串转化为时间"+date);
    System.out.println("获取小时"+DateUtil.hour(date,true));
         System.out.println("获取年"+DateUtil.year(date));
    System.out.println("时间间隔"+DateUtil.between(date,new Date(), DateUnit.MINUTE));
         Date Nowdate=new Date();
         Calendar calendar=Calendar.getInstance();
         calendar.setTime(Nowdate);
         calendar.add(Calendar.DAY_OF_MONTH,-1);
         Date dateSub= calendar.getTime();
         String nowTime=DateUtil.format(dateSub,"yyyy-MM-dd");
    System.out.println("包含的时间"+nowTime);
     }
     public static   void  testString(){
         String str[]={"a","b","c","d","e"};
       List list=  Convert.toList(str);
    System.out.println("数组转化后的集合"+list);

     }
     public static  void  testMd5(){
         String number="123456";
       String md=  SecureUtil.md5(number);
    System.out.println(md);
          if(md.equals("e10adc3949ba59abbe56e057f20f883e")){
             System.out.println("一致");
          }
     }

     public static  void testHttp(){
        String result=  HttpUtil.get("https://api-dev.yeahgo.com/cms/open/banks/findAllBanks");
    System.out.println("http请求的结果"+result);
     }

     //生成文件
    public static String     testQrCode(){

    String targetPath = "D:\\myitem\\mqitem\\common\\src\\main\\resources\\static\\"+ UUID.randomUUID().toString() +".png";
    QrCodeUtil.generate( //
        "https://dev-yeahgo.oss-cn-shenzhen.aliyuncs.com/android_apk/yeahgo_v1.0.4_debug.apk", // 二维码内容
        QrConfig.create().setBackColor(Color.red).setRatio(1), // 附带logo
        FileUtil.file(targetPath) // 写出到的文件
        );
        String finalPath=targetPath.substring(targetPath.lastIndexOf("\\")+1);
    log.info("最后的路径{}",finalPath);
    return  finalPath;
    }
    /**
     * 生成到流
     *
     * @param content
     * @param response
     */
    public static void createQRCode2Stream(String content, HttpServletResponse response) {
        try {
            QrCodeUtil.generate(content,  initQrConfig(),"png", response.getOutputStream());
            log.info("生成二维码成功!");
        } catch (QrCodeException | IOException e) {
            log.error("发生错误！ {}！", e.getMessage());
        }
    }

  public static void main(String[] args) {
//      testDate();
//      testString();
//      testMd5();
//      testHttp();
      testQrCode();
  }
}
