package app.controller;

import app.entity.ImgEntity;
import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/11/23.
 */
@Controller
public class ImgController {
    @PostMapping("/addImages")
    @ResponseBody
    public Boolean addImages(@RequestBody ImgEntity imgEntity){
        Boolean flag=false;
        if(imgEntity!=null){
            System.out.println(imgEntity.toString());
            flag=true;
        }
          return     flag;
    }
}
