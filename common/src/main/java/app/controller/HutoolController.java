package app.controller;

import app.utils.TestHutool;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/11/16.
 */
@Controller
public class HutoolController {
    @GetMapping("/qrCode")
    public  String hutool(HttpServletRequest request){
        return "qrCode";

    }
    @GetMapping("/test")
    @ResponseBody
    public String test(){
        return "测试";
    }
    @PostMapping("/saveQrCode")
    public String saveQrCode(HttpSession session, HttpServletResponse response){
      String  codeImg=  TestHutool.testQrCode();
        session.setAttribute("codeImg",codeImg);
       // TestHutool.createQRCode2Stream("我的二维码",response);
        return "qrCode";
    }
    //另一种生成二维码的方式
    @GetMapping("/QrTest")
    public void get(HttpServletResponse response) throws Exception {
        int width = 200;
        int height = 200;
        String format = "png";
        String content = "jwttttt";
        ServletOutputStream out = response.getOutputStream();
        Map<EncodeHintType,Object> config = new HashMap<>();
        config.put(EncodeHintType.CHARACTER_SET,"UTF-8");
        config.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        config.put(EncodeHintType.MARGIN, 0);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE,width,height,config);
        MatrixToImageWriter.writeToStream(bitMatrix,format,out);
        System.out.println("二维码生成完毕，已经输出到页面中。");
    }

}
