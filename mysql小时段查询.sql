
-- mysql查询时间段的数据

SELECT
	CASE
WHEN HOUR (create_time) BETWEEN 0
AND 3 THEN
	'00'
WHEN HOUR (create_time) BETWEEN 3
AND 6 THEN
	'03'
WHEN HOUR (create_time) BETWEEN 6
AND 9 THEN
	'06'
WHEN HOUR (create_time) BETWEEN 9
AND 12 THEN
	'09'
WHEN HOUR (create_time) BETWEEN 12
AND 15 THEN
	'12'
WHEN HOUR (create_time) BETWEEN 15
AND 18 THEN
	'15'
WHEN HOUR (create_time) BETWEEN 18
AND 21 THEN
	'18'
WHEN HOUR (create_time) BETWEEN 21
AND 23 THEN
	'21'
END AS `time`,
 IFNULL(COUNT(1), 0) AS `count`,
 sum(pay_amount) AS pay
FROM
	order_db.t_order_sum
WHERE
	1 = 1 -- 这里可以添加其他条件 比如 and flag=1 and type = 2 and isdelete = 0
	-- AND to_Days(DISCOVERTIME) = to_days(now())  这里不建议这么写，有to_days()这种运算的逻辑可以放到程序中去处理，没必要在数据库中去做运算，我们在开发过程中尽量避免这一点，可以参考下面这种写法，这个时间的处理可以参考我的另外一篇博客 http://www.fujiatian.com/post/24365.html
	-- and	create_time >= '2019-11-11 00:00:00' AND create_time < '2019-11-12 00:00:00'
GROUP BY
	CASE
WHEN HOUR (create_time) BETWEEN 0
AND 3 THEN
	1
WHEN HOUR (create_time) BETWEEN 3
AND 6 THEN
	2
WHEN HOUR (create_time) BETWEEN 6
AND 9 THEN
	3
WHEN HOUR (create_time) BETWEEN 9
AND 12 THEN
	4
WHEN HOUR (create_time) BETWEEN 12
AND 15 THEN
	5
WHEN HOUR (create_time) BETWEEN 15
AND 18 THEN
	6
WHEN HOUR (create_time) BETWEEN 18
AND 21 THEN
	7
WHEN HOUR (create_time) BETWEEN 21
AND 23 THEN
	8
END