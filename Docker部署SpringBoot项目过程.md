# Docker部署SpringBoot项目

![img](https://upload.jianshu.io/users/upload_avatars/15536448/685ab438-e2ec-4794-b921-f4a99a725e54.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/96/h/96/format/webp)

[雄关漫道从头越](https://www.jianshu.com/u/cd18446dfb5f)关注

22019.01.20 02:46:03字数 432阅读 189,398

> [使用docker部署SpringBoot项目](https://links.jianshu.com/go?to=https%3A%2F%2Fblog.csdn.net%2Fjunmoxi%2Farticle%2Fdetails%2F80861199)
> [springboot整合docker部署（两种构建Docker镜像方式）](https://links.jianshu.com/go?to=https%3A%2F%2Fwww.cnblogs.com%2Fshamo89%2Fp%2F9201513.html)
>
> Get https://registry-1.docker.io/v2/: net/http: TLS handshake timeout（Docker镜像拉取错误）
>
> 白不懂黑的静 2020-05-01 11:20:04  5050  收藏 5
> 分类专栏： docker
> 版权
>
> docker
> 专栏收录该内容
> 12 篇文章0 订阅
> 订阅专栏
> 场景：
>
> 在使用docker 拉取httpd的镜像时，报连接超时的问题，如标题
>
>  
>
> 处理办法：
>
> 修改或新建/ect/docker/daemon.json文件
>
> sudo vim daemon.json
> 文件中编辑阿里云的镜像地址：
>
> {
>  "registry-mirrors":["https://6kx4zyno.mirror.aliyuncs.com"]
> }
> 重启docker服务（注意：必须重启，否则不生效）
>
> systemctl daemon-reload 
> systemctl restart docker
> 然后再次拉取：
> ————————————————
> 版权声明：本文为CSDN博主「白不懂黑的静」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
> 原文链接：https://blog.csdn.net/yyj108317/article/details/105875582/
>
> ## [Spring Boot与Docker部署](https://www.cnblogs.com/java-my-life/p/7001998.html)
>
> ## 开启Docker远程访问
>
> 　　首先需要开启docker远程访问功能，以便可以进行远程操作。
>
> - #### CentOS 6
>
> 　　修改/etc/default/docker文件，重启后生效（service docker restart）。
>
> ```
> DOCKER_OPTS="-H=unix:///var/run/docker.sock -H=0.0.0.0:2375"
> ```
>
> - ####  CentOS 7
>
> 　　打开/usr/lib/systemd/system/docker.service文件，修改ExecStart这行。
>
> ```
> ExecStart=/usr/bin/dockerd  -H tcp://0.0.0.0:2375  -H unix:///var/run/docker.sock
> ```
>
> 　　重启后生效
>
> ```
>   systemctl daemon-reload    
>   systemctl restart docker.service 
> ```
>
> 　　测试是否生效
>
> ```
> curl http://127.0.0.1:2375/info
> ```
>
> ##  
>
> ------
>
> ##  
>
> ## 新建Maven工程
>
> 　　pom.xml配置如下:
>
> [![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)
>
> ```
> <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>     xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
>     <modelVersion>4.0.0</modelVersion>
>     <groupId>test.springboot.docker</groupId>
>     <artifactId>docker-springboot</artifactId>
>     <version>0.0.1-SNAPSHOT</version>
>
>     <parent>
>         <groupId>org.springframework.boot</groupId>
>         <artifactId>spring-boot-starter-parent</artifactId>
>         <version>1.5.3.RELEASE</version>
>     </parent>
>
>     <properties>
>         <java.version>1.8</java.version>
>     </properties>
>
>     <dependencies>
>         <dependency>
>             <groupId>org.springframework.boot</groupId>
>             <artifactId>spring-boot-starter-web</artifactId>
>         </dependency>
>         <dependency>
>             <groupId>org.springframework.boot</groupId>
>             <artifactId>spring-boot-starter-test</artifactId>
>             <scope>test</scope>
>         </dependency>
>     </dependencies>
>
>     <build>
>         <plugins>
>             <plugin>
>                 <groupId>org.springframework.boot</groupId>
>                 <artifactId>spring-boot-maven-plugin</artifactId>
>             </plugin>
>             <plugin>
>                 <groupId>com.spotify</groupId>
>                 <artifactId>docker-maven-plugin</artifactId>
>                 <version>0.4.14</version>
>                 <configuration>
>                     <imageName>${docker.image.prefix}/${project.artifactId}</imageName>
>                     <dockerDirectory>src/main/docker</dockerDirectory>
>                     <dockerHost>http://192.168.1.200:2375</dockerHost>
>                     <resources>
>                         <resource>
>                             <targetPath>/</targetPath>
>                             <directory>${project.build.directory}</directory>
>                             <include>${project.build.finalName}.jar</include>
>                         </resource>
>                     </resources>
>                 </configuration>
>             </plugin>
>         </plugins>
>     </build>
> </project>
> ```
>
> [![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)
>
> `imageName：`指定了镜像的名字
>
> `dockerDirectory：`指定Dockerfile的位置
>
> `dockerHost：`指定Docker远程API地址
>
> `resources：`指那些需要和Dockerfile放在一起，在构建镜像时使用的文件，一般应用jar包需要纳入
>
> #### 　　创建Java类
>
> [![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)
>
> ```
> package hello;
>
> import org.springframework.boot.SpringApplication;
> import org.springframework.boot.autoconfigure.SpringBootApplication;
> import org.springframework.web.bind.annotation.RequestMapping;
> import org.springframework.web.bind.annotation.RestController;
>
> @SpringBootApplication
> @RestController
> public class Application {
>
>     @RequestMapping("/")
>     public String home() {
>         return "Hello Docker World";
>     }
>
>     public static void main(String[] args) {
>         SpringApplication.run(Application.class, args);
>     }
>
> }
> ```
>
> [![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)
>
> #### 　　创建Dockerfile
>
> 　　在src/main/docker目录下创建一个名为Dockerfile的文件，配置如下：
>
> [![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)
>
> ```
> FROM java
> VOLUME /tmp
> ADD docker-springboot-0.0.1-SNAPSHOT.jar app.jar
> RUN bash -c 'touch /app.jar'
> ENV JAVA_OPTS=""
> ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
> ```
>
> [![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)
>
> **FROM java：**指Docker Hub上官方提供的java镜像，有了这个基础镜像后，Dockerfile可以通过`FROM`指令直接获取它的状态——也就是在容器中`java`是已经安装的，接下来通过自定义的命令来运行Spring Boot应用。
>
> **VOLUME /tmp：**创建/tmp目录并持久化到Docker数据文件夹，因为Spring Boot使用的内嵌Tomcat容器默认使用`/tmp`作为工作目录。
>
> **ADD docker-springboot-0.0.1-SNAPSHOT.jar app.jar：**将应用jar包复制到`/app.jar`
>
> **ENTRYPOINT：**表示容器运行后默认执行的命令
>
> **完整目录结构如下所示：**
>
> **![img](https://images2015.cnblogs.com/blog/171196/201706/171196-20170614163650196-291529205.png)**
>
> 　　运行以下命令创建Docker镜像：
>
> ```
> package docker:build
> ```

### 1.创建springboot项目

![img](https://upload-images.jianshu.io/upload_images/15536448-022dcb5d56f43d70.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

创建springboot项目

```
package com.eangulee.demo.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
    @RequestMapping("/")
    @ResponseBody
    public String hello() {
        return "Hello, SpringBoot With Docker";
    }
}
```

### 2.打包springboot项目为jar包

![img](https://upload-images.jianshu.io/upload_images/15536448-30b7e78666cc0094.png?imageMogr2/auto-orient/strip|imageView2/2/w/1099/format/webp)

### 3. 编写Dockerfile文件

```
# Docker image for springboot file run
# VERSION 0.0.1
# Author: eangulee
# 基础镜像使用java
FROM java:8
# 作者
MAINTAINER eangulee <eangulee@gmail.com>
# VOLUME 指定了临时文件目录为/tmp。
# 其效果是在主机 /var/lib/docker 目录下创建了一个临时文件，并链接到容器的/tmp
VOLUME /tmp 
# 将jar包添加到容器中并更名为app.jar
ADD demo-0.0.1-SNAPSHOT.jar app.jar 
# 运行jar包
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

**解释下这个配置文件：**

VOLUME 指定了临时文件目录为/tmp。其效果是在主机 /var/lib/docker 目录下创建了一个临时文件，并链接到容器的/tmp。该步骤是可选的，如果涉及到文件系统的应用就很有必要了。/tmp目录用来持久化到 Docker 数据文件夹，因为 Spring Boot 使用的内嵌 Tomcat 容器默认使用/tmp作为工作目录
项目的 jar 文件作为 “app.jar” 添加到容器的
ENTRYPOINT 执行项目 app.jar。为了缩短 Tomcat 启动时间，添加一个系统属性指向 “/dev/./urandom” 作为 Entropy Source

如果是第一次打包，它会自动下载java 8的镜像作为基础镜像，以后再制作镜像的时候就不会再下载了。

### 4. 部署文件

在服务器新建一个docker文件夹，将maven打包好的jar包和Dockerfile文件复制到服务器的docker文件夹下

![img](https://upload-images.jianshu.io/upload_images/15536448-9af07900b7686a79.png?imageMogr2/auto-orient/strip|imageView2/2/w/360/format/webp)

docker文件夹

### 5. 制作镜像

执行下面命令， 看好，最后面有个"."点！

```
docker build -t springbootdemo4docker .
```

-t 参数是指定此镜像的tag名

![img](https://upload-images.jianshu.io/upload_images/15536448-d5087e016f87a13a.png?imageMogr2/auto-orient/strip|imageView2/2/w/729/format/webp)

制作完成后通过**docker images**命令查看我们制作的镜像

![img](https://upload-images.jianshu.io/upload_images/15536448-4a8a43c82addd15e.png?imageMogr2/auto-orient/strip|imageView2/2/w/882/format/webp)

### 6.启动容器

```
[root@localhost docker]# docker run -d -p 8080:8085 springbootdemo4docker
-d参数是让容器后台运行 
-p 是做端口映射，此时将服务器中的8080端口映射到容器中的8085(项目中端口配置的是8085)端口
```

### 7. 访问网站

直接浏览器访问： http://你的服务器ip地址:8080/