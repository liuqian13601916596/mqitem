/*
Navicat MySQL Data Transfer

Source Server         : 虚拟
Source Server Version : 80026
Source Host           : 192.168.254.134:3306
Source Database       : pay

Target Server Type    : MYSQL
Target Server Version : 80026
File Encoding         : 65001

Date: 2021-10-20 16:39:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名',
  `price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `stock` int NOT NULL COMMENT '库存',
  `sale` int NOT NULL COMMENT '售卖数量',
  `version` int NOT NULL COMMENT '乐观锁版本号',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1418094542299299842 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1418093506813636609', '桃子', '3.00', '10', '6', '0', '2021-07-22 14:20:14', '2021-07-22 14:20:14');
INSERT INTO `goods` VALUES ('1418093827648532481', '李子', '5.00', '10', '8', '3', '2021-07-22 14:21:30', '2021-07-22 14:21:30');
INSERT INTO `goods` VALUES ('1418093977980862465', '苹果', '5.00', '10', '5', '0', '2021-07-22 14:22:06', '2021-07-22 14:22:06');
INSERT INTO `goods` VALUES ('1418094215093256194', '葡萄', '12.00', '10', '5', '0', '2021-07-22 14:23:02', '2021-07-22 14:23:02');
INSERT INTO `goods` VALUES ('1418094542299299841', '橘子', '3.00', '25', '15', '0', '2021-07-22 14:24:20', '2021-07-22 14:40:45');

-- ----------------------------
-- Table structure for money
-- ----------------------------
DROP TABLE IF EXISTS `money`;
CREATE TABLE `money` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `uid` bigint DEFAULT NULL COMMENT '用户id',
  `amount` decimal(10,0) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of money
-- ----------------------------
INSERT INTO `money` VALUES ('15', '3', '58', '2021-09-22 09:38:10', '2021-09-28 11:00:20');
INSERT INTO `money` VALUES ('16', '4', '162', '2021-09-22 09:38:10', '2021-09-28 11:00:20');
INSERT INTO `money` VALUES ('17', '5', '120', '2021-10-11 09:08:59', '2021-10-11 09:08:59');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '订单Id',
  `user_id` bigint NOT NULL COMMENT '用户Id',
  `goods_id` bigint NOT NULL COMMENT '商品Id',
  `price` decimal(10,0) NOT NULL,
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1442685578493820931 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1440610957745504257', '3', '1418094215093256194', '12', '2021-09-22 17:36:32', '2021-09-22 17:36:32');
INSERT INTO `orders` VALUES ('1440611437649379329', '3', '1418094215093256194', '12', '2021-09-22 17:38:27', '2021-09-22 17:38:27');
INSERT INTO `orders` VALUES ('1440611852386349057', '3', '1418094215093256194', '12', '2021-09-22 17:40:06', '2021-09-22 17:40:06');
INSERT INTO `orders` VALUES ('1440612076840333314', '3', '1418094215093256194', '12', '2021-09-22 17:40:59', '2021-09-22 17:40:59');
INSERT INTO `orders` VALUES ('1440630613512318978', '3', '1418094215093256194', '3', '2021-09-22 18:54:39', '2021-09-22 18:54:39');
INSERT INTO `orders` VALUES ('1440631545637699586', '3', '1418094215093256194', '12', '2021-09-22 18:58:21', '2021-09-22 18:58:21');
INSERT INTO `orders` VALUES ('1440632022840455170', '3', '1418094215093256194', '12', '2021-09-22 19:00:15', '2021-09-22 19:00:15');
INSERT INTO `orders` VALUES ('1440883147799859201', '3', '1418094542299299841', '10', '2021-09-23 11:38:07', '2021-09-23 11:38:07');
INSERT INTO `orders` VALUES ('1440887145290317825', '3', '1418094215093256194', '3', '2021-09-23 11:54:00', '2021-09-23 11:54:00');
INSERT INTO `orders` VALUES ('1442685578493820930', '3', '1418094542299299841', '10', '2021-09-28 11:00:20', '2021-09-28 11:00:20');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rid` bigint DEFAULT NULL COMMENT '角色id',
  `permissionName` varchar(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '1', 'add', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('2', '1', 'update', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('3', '1', 'delete', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('4', '1', 'select', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('5', '2', 'add', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('6', '2', 'update', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('7', '2', 'select', '2021-09-29 13:45:53', '2021-09-29 13:46:27');
INSERT INTO `permission` VALUES ('8', '3', 'select', '2021-09-29 13:45:53', '2021-09-29 13:46:27');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `uid` bigint DEFAULT NULL,
  `roleName` varchar(20) DEFAULT NULL,
  `gmt_create` datetime(1) DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '3', '超级管理员', '2021-09-29 13:46:42.0', '2021-09-29 13:46:49');
INSERT INTO `role` VALUES ('2', '4', '会员', '2021-09-29 13:46:42.0', '2021-09-29 13:46:49');
INSERT INTO `role` VALUES ('3', '5', '游客', '2021-09-29 13:46:42.0', '2021-09-29 13:46:49');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户Id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `usernam` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1418037588084371459 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('3', 'ljs', 'ljs123', '123456', '2021-07-21 10:29:52', '2021-07-21 10:29:52');
INSERT INTO `user` VALUES ('4', '张三', '张三123', '123456', '2021-07-21 10:31:06', '2021-07-21 10:31:06');
INSERT INTO `user` VALUES ('5', '李四', '李四123', '123456', '2021-07-21 10:31:17', '2021-07-21 10:31:17');
INSERT INTO `user` VALUES ('6', '王老五', '老五123', '123456', '2021-07-21 10:31:29', '2021-07-21 10:31:29');
INSERT INTO `user` VALUES ('7', '666', '老6123', '123456', '2021-07-21 10:37:56', '2021-07-21 10:37:56');
INSERT INTO `user` VALUES ('8', '老777', '老7776123', '123456', '2021-07-21 10:39:21', '2021-07-21 10:39:21');
INSERT INTO `user` VALUES ('1417677865887600642', '老777', '老7776123', '123456', '2021-07-21 10:48:37', '2021-07-21 10:48:37');
INSERT INTO `user` VALUES ('1417677865887600643', '小红', '小红123', '123', '2021-07-21 11:24:21', '2021-07-21 21:37:19');
INSERT INTO `user` VALUES ('1417677865887600644', '小明', '小明123', '123', '2021-07-21 11:24:21', '2021-07-21 21:37:19');
INSERT INTO `user` VALUES ('1417677865887600645', '小华', '小华123', '123', '2021-07-21 11:24:21', '2021-07-21 21:37:19');
INSERT INTO `user` VALUES ('1418037588084371457', '小子', '小子123', '123456', '2021-07-22 10:38:02', '2021-07-22 10:38:02');
INSERT INTO `user` VALUES ('1418037588084371458', 'import', 'import123', '123456', '2021-07-21 02:29:52', '2021-07-21 02:29:52');
