package oauth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/9/29.
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("oauth.orm.mapper")
@EnableTransactionManagement
public class OauthApplication {
  public static void main(String[] args) {
    SpringApplication.run(OauthApplication.class,args);
  }
}
