package oauth.api;


import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.UserStock;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2022-01-12
 */
public interface UserStockService extends IService<UserStock> {

}
