package oauth.api;


import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.User;
import oauth.vo.UserRequest;
import oauth.vo.UserResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
public interface UserService extends IService<User> {
       User findByName(String name);

       UserResponse findUser(UserRequest userRequest);

}
