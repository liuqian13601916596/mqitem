package oauth.api;


import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.Money;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
public interface MoneyService extends IService<Money> {

}
