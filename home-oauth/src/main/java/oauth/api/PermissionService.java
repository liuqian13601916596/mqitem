package oauth.api;


import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.Permission;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
public interface PermissionService extends IService<Permission> {
    /**
     * 根据角色查相应的权限
     * @param RoleId
     * @return
     */
   List<Permission> findByRoleId(String RoleId);
}
