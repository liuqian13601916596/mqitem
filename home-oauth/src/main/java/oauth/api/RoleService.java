package oauth.api;


import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.Role;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
public interface RoleService extends IService<Role> {
    //根据用户id查询角色,一个用户对应一种角色
    Role findByUid(String uid);
}
