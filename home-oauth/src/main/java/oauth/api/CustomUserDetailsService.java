package oauth.api;

import javax.annotation.Resource;

/**
 * @Description:实现springsecurity的用户详情服务
 * @Author: liujinsheng
 * @Date: 2021/10/11.
 */

public class CustomUserDetailsService  {
    @Resource
    UserService userService;
    @Resource
    RoleService roleService;
    @Resource
    PermissionService permissionService;
    /**
     * 密码加密
     */
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//       User user= userService.findByName(username);
//       if(user==null){
//           throw new UsernameNotFoundException("用户不存在");
//       }
//        //声明一个用于存放用户权限的列表
//        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
//       //获取角色
//        Role role=roleService.findByUid(user.getId()+"");
//        if(role!=null){
//            //注意：添加角色的时候要在前面加ROLE_前缀
//            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+role.getRoleName()));
//            List<Permission> permissionList=permissionService.findByRoleId(role.getId()+"");
//            permissionList.forEach(permission -> {
//                grantedAuthorities.add(new SimpleGrantedAuthority(permission.getPermissionName()));
//            });
//        }
//        return new org.springframework.security.core.userdetails.User(user.getUsernam(),user.getPassword(),grantedAuthorities);
//    }
}
