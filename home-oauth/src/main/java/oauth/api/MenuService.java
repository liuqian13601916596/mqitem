package oauth.api;

import app.response.MenuResponse;

import java.util.List;

public interface MenuService {
    public List<MenuResponse> getMenu();
}
