package oauth.api;


import app.request.OrderRequest;
import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.Order;
import oauth.util.AjaxResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
public interface OrderService extends IService<Order> {
    public AjaxResult payOrder(OrderRequest orderRequest);

}
