package oauth.api;

import app.request.BaseRollPageRequest;
import app.request.GoodsListRequest;
import app.request.GoodsPageRequest;
import app.request.GoodsRequest;
import app.response.BasePageResult;
import app.response.BaseRollPageResult;
import app.utils.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import oauth.orm.entity.Goods;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
public interface GoodsService extends IService<Goods> {
    public Result<Boolean> updateMysqlStock(GoodsRequest goodsRequest);
    public Result<Boolean> updateRedisStock(GoodsRequest goodsRequest);
    public void initStock();
    public Result<Map<String,Goods >> getGoods(GoodsListRequest goodsListRequest);

    /**
     * 分页查询
     * @param goodsPageRequest
     * @return
     */
    public Result<BasePageResult<Goods>> findByWays(GoodsPageRequest goodsPageRequest);

    /**
     * 滚动查询
     * @return
     */
    public Result<BaseRollPageResult<Goods>> scroll(BaseRollPageRequest baseRollPageRequest);

}
