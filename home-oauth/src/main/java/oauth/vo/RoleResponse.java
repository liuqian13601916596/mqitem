package oauth.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import oauth.orm.entity.Permission;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long roleId;

    private Long uid;
    private String roleName;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
    /**
     * 权限
     */
    private List<Permission> permissionList;

}