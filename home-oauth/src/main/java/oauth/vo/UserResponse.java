package oauth.vo;

import lombok.Data;
import oauth.orm.entity.Permission;
import oauth.orm.entity.Role;

import java.io.Serializable;
import java.util.List;

@Data
public class UserResponse implements Serializable {
        private Long id;
        /**
         * 姓名
         */
        private String name;
        /**
         * 角色
         */
        private Role role;
        /**
         * 权限
         */
        private List<Permission> permissionList;
}
