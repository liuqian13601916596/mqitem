package oauth.vo;

import lombok.Data;
import oauth.orm.entity.User;

import java.io.Serializable;
import java.util.List;
@Data
public class UserListRequest implements Serializable {
  private   List<Long> idList;
  private List<User> userList;
}
