package oauth.orm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import oauth.orm.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {
    public Boolean updateStock(@Param("goodsId") String goodsId, @Param("stock") int stock);

}
