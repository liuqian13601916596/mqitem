package oauth.orm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import oauth.orm.entity.User;
import oauth.vo.UserRequest;
import oauth.vo.UserResponse;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
public interface UserMapper extends BaseMapper<User> {
    UserResponse findUser(UserRequest userRequest);

}
