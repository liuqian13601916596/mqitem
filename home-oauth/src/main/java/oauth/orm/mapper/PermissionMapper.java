package oauth.orm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import oauth.orm.entity.Permission;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
public interface PermissionMapper extends BaseMapper<Permission> {
    List<Permission> findByRoleId(String RoleId);
}
