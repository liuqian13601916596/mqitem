package oauth.orm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import oauth.orm.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
public interface RoleMapper extends BaseMapper<Role> {

}
