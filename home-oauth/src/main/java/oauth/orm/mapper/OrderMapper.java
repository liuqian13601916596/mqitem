package oauth.orm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import oauth.orm.entity.Order;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
public interface OrderMapper extends BaseMapper<Order> {

}
