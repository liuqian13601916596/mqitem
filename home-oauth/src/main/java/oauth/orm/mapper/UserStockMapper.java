package oauth.orm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import oauth.orm.entity.UserStock;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2022-01-12
 */
@Mapper
public interface UserStockMapper extends BaseMapper<UserStock> {

}
