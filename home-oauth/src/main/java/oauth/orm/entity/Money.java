package oauth.orm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Money implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private Long uid;

    private BigDecimal amount;

    private LocalDateTime gmtCreate;

    private LocalDateTime gmtModified;


}
