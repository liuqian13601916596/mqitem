package oauth.orm.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/11/6.
 */
@Data
public class Routes implements Serializable {
    private String mname;
    private String name;
    private String routeUrl;
    private  String param;
    private String remark;
}
