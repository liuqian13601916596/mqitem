package oauth.orm.entity;

import com.alibaba.excel.annotation.ExcelProperty;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import app.config.LocalDateTimeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户Id
     */
    @ExcelProperty(value = "id")
    @TableId( type = IdType.ID_WORKER)
    private Long userId;

    /**
     * 姓名
     */
    @ExcelProperty("名称")
    private String name;

    /**
     * 用户名
     */
    @ExcelProperty("用户名")
    private String userName;

    /**
     * 密码
     */
    @ExcelProperty("密码")
    private String password;

    /**
     * 创建时间,用fastjson,才能转化给前端格式化
     */
    @ExcelProperty(value = "创建时间",converter = LocalDateTimeConverter.class)

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    @TableField(fill= FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtCreate;

    /**
     * 修改时间，转化给前端格式化,easyExcel不能转化localdatatime,要进行转化
     */
    @ExcelProperty(value = "修改时间",converter = LocalDateTimeConverter.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    @TableField(fill= FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;
    /*逻辑删除字段*/
    @TableLogic
    private Integer deleted;


}
