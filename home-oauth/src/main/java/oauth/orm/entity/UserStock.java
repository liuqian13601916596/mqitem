package oauth.orm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljs
 * @since 2022-01-12
 */
@Data
@TableName("userStock")
public class UserStock implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @TableField("userId")
    private String userId;

    @TableField("goodsId")
    private String goodsId;


}
