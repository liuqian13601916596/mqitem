package oauth.config;

import lombok.Data;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/10/12.
 */
@Data
public class Response {
    private  String msg;
    private Integer code;
    private  boolean bln;
}
