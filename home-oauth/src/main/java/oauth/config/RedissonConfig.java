package oauth.config;


import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.MasterSlaveServersConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:redisson配置
 * @Author: liujinsheng
 * @Date: 2021/3/30.
 */

@Configuration
@Slf4j
public class RedissonConfig {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password}")
    private String password;

    @Value("${spring.redis.database}")
    private int database;

   @Bean
    public RedissonClient getRedisson() {

        Config config = new Config();
        String address1 = "redis://" + host + ":" + port;
       //单机模式
       config.useSingleServer().setAddress(address1).setPassword(password).setDatabase(database);
       //RedissonClient redisson = Redisson.create(config);
       //主从复制模式
       //主地址
      // String address1 = "redis://" + host + ":" + port;
       //从地址
//       String address2="redis://192.168.254.134:6379";
//       config.useMasterSlaveServers().setMasterAddress(address1).setPassword(password).setDatabase(database).addSlaveAddress(address2).setPassword(null).setDatabase(0);

        RedissonClient redisson = Redisson.create(config);
          redisson.getBucket("masterSlaveClient").set("masterSlaveClient");

        return redisson;

    }
}

