package oauth.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.net.UnknownHostException;

@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) throws UnknownHostException {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }


//    @Bean
//    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
//        RedisSerializer<Object> serializer = redisSerializer();
//        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
//        redisTemplate.setConnectionFactory(redisConnectionFactory);
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        redisTemplate.setValueSerializer(serializer);
//        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//        redisTemplate.setHashValueSerializer(serializer);
//        redisTemplate.afterPropertiesSet();
//        return redisTemplate;
//    }
//
//    @Bean
//    public RedisSerializer<Object> redisSerializer() {
//        //创建JSON序列化器
//        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        //必须设置，否则无法将JSON转化为对象，会转化成Map类型
//        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,ObjectMapper.DefaultTyping.NON_FINAL);
//        serializer.setObjectMapper(objectMapper);
//        return serializer;
//    }

//    @Bean
//    RedisMessageListenerContainer homeContainer(RedisConnectionFactory redisConnectionFactory,
//                                            MessageListenerAdapter listenerVersionAdapter) {
//
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(redisConnectionFactory);
//        // 发布/订阅版本更新, 多个多次调用 多个容器调用
//        container.addMessageListener(listenerVersionAdapter, new PatternTopic(SystemConstant.TopicType.HOME_VERSION_TOPIC));
//        return container;
//    }
//
//    /**
//     * 资源位
//     * @param redisConnectionFactory
//     * @param listenerResourceAdapter
//     * @return
//     */
//    @Bean
//    RedisMessageListenerContainer resourceContainer(RedisConnectionFactory redisConnectionFactory,
//                                            MessageListenerAdapter listenerResourceAdapter) {
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(redisConnectionFactory);
//        container.addMessageListener(listenerResourceAdapter, new PatternTopic(SystemConstant.resourceTopic.resourceTopic));
//        return container;
//    }
//
//
//    @Bean
//    RedisMessageListenerContainer BannerContainer(RedisConnectionFactory redisConnectionFactory,
//                                                    MessageListenerAdapter listenerBannerAdapter) {
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(redisConnectionFactory);
//        container.addMessageListener(listenerBannerAdapter, new PatternTopic(SystemConstant.TopicType.HOME_BANNER_TOPIC));
//        return container;
//    }
//
//
//    @Bean
//    RedisMessageListenerContainer ActivityConfigContainer(RedisConnectionFactory redisConnectionFactory,
//                                                  MessageListenerAdapter listenerActivityConfigAdapter) {
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(redisConnectionFactory);
//        container.addMessageListener(listenerActivityConfigAdapter, new PatternTopic(SystemConstant.ActivityIconConfigTopic.activityConfigTopic));
//        return container;
//    }
//
////    @Bean
////    RedisMessageListenerContainer BanksContainer(RedisConnectionFactory redisConnectionFactory,
////                                                    MessageListenerAdapter listenerResourceAdapter) {
////        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
////        container.setConnectionFactory(redisConnectionFactory);
////        container.addMessageListener(listenerResourceAdapter, new PatternTopic(SystemConstant.BanksTopic.BanksTopic));
////        return container;
////    }
//
//    @Bean
//    MessageListenerAdapter listenerVersionAdapter(RedisMessageHomeReceiver redisMessageHomeReceiver) {
//        return new MessageListenerAdapter(redisMessageHomeReceiver, "receiveMessage");
//    }
//
//    /**
//     * 资源位的监听
//     *
//     * @param redisMessageResourceReceiver
//     * @return
//     */
//    @Bean
//    MessageListenerAdapter listenerResourceAdapter(RedisMessageResourceReceiver redisMessageResourceReceiver) {
//        return new MessageListenerAdapter(redisMessageResourceReceiver, "receiveResourceMessage");
//    }
//
//    @Bean
//    MessageListenerAdapter listenerBannerAdapter(RedisMessageBannerReceiver redisMessageBannerReceiver) {
//        return new MessageListenerAdapter(redisMessageBannerReceiver, "receiveMessage");
//    }
//
////    @Bean
////    MessageListenerAdapter listenerBanksAdapter(RedisMessageBannerReceiver redisMessageBannerReceiver) {
////        return new MessageListenerAdapter(redisMessageBannerReceiver, "receiveMessage");
////    }
//
//    @Bean
//    MessageListenerAdapter listenerActivityConfigAdapter(RedisActivityIconConfigReceiver redisActivityIconConfigReceiver) {
//        return new MessageListenerAdapter(redisActivityIconConfigReceiver, "receiveMessage");
//    }
}
