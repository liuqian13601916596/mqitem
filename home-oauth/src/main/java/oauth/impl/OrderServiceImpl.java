package oauth.impl;

import app.request.OrderRequest;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import oauth.orm.entity.Order;
import oauth.orm.mapper.OrderMapper;
import oauth.api.OrderService;
import oauth.util.AjaxResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Override
    public AjaxResult payOrder(OrderRequest orderRequest) {
        if(StringUtils.isBlank(orderRequest.getUserId())){
            return AjaxResult.error("用户id不能为空");
        }
        if(StringUtils.isBlank(orderRequest.getGoodsId())){
            return AjaxResult.error("商品id不能为空");
        }
        if(orderRequest.getPayNum()==null){
            return AjaxResult.error("购买个数不能为空");
        }
        return null;
    }
}
