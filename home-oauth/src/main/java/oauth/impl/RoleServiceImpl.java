package oauth.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import oauth.api.RoleService;
import oauth.orm.entity.Role;
import oauth.orm.mapper.RoleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
     @Resource
     RoleMapper roleMapper;
    @Override
    public Role findByUid(String uid) {
        QueryWrapper<Role> queryWrapper=new QueryWrapper();
        queryWrapper.lambda().eq(Role::getUid,uid);
        return   roleMapper.selectOne(queryWrapper);
    }
}
