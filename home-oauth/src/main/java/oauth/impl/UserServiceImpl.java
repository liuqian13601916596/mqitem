package oauth.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import oauth.orm.entity.User;
import oauth.orm.mapper.UserMapper;
import oauth.api.UserService;
import oauth.vo.UserRequest;
import oauth.vo.UserResponse;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类.
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Resource
    UserMapper userMapper;

    @Override
    public User findByName(String name) {
        QueryWrapper<User> queryWrapper=new QueryWrapper();
        queryWrapper.lambda().eq(User::getName,name);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public UserResponse findUser(UserRequest userRequest) {
        return userMapper.findUser(userRequest);
    }
}
