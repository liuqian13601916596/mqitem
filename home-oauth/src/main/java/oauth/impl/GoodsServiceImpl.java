package oauth.impl;

import app.request.BaseRollPageRequest;
import app.request.GoodsListRequest;
import app.request.GoodsPageRequest;
import app.request.GoodsRequest;
import app.response.BasePageResult;
import app.response.BaseRollPageResult;
import app.utils.util.Result;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import oauth.api.GoodsService;
import oauth.biz.GoodsBiz;
import oauth.orm.entity.Goods;
import oauth.orm.mapper.GoodsMapper;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {
   @Resource
   GoodsBiz goodsBiz;
    @Resource
    RedissonClient redisson;
    @Resource
    RedisTemplate<String, String> redisTemplate;

    @Override
    public Result<Boolean> updateMysqlStock(GoodsRequest goodsRequest) {

        if(StringUtils.isBlank(goodsRequest.getUserId())){
            return  Result.ofFailMsg("用户id不能为空");
        }
        if(StringUtils.isBlank(goodsRequest.getGoodsId())){
            return  Result.ofFailMsg("商品id不能为空");
        }


        return goodsBiz.updateMysqlStock(goodsRequest);


    }

    @Override
    public Result<Boolean> updateRedisStock(GoodsRequest goodsRequest) {
        return goodsBiz.updateRedisStock(goodsRequest);
    }

    @Override
    public void initStock() {
        //启动完设置默认的库存
        String stockKey="mm:goods:stock:"+"1418094542299299841";
        //测试设置库存为10
        redisTemplate.opsForValue().set(stockKey, JSON.toJSONString(10),5, TimeUnit.MINUTES);
    }

    @Override
    public Result<Map<String,Goods >> getGoods(GoodsListRequest goodsListRequest) {
        if(CollectionUtils.isEmpty(goodsListRequest.getIdList())){
            return Result.ofFailMsg("id不能为空");

        }
        return goodsBiz.getGoods(goodsListRequest.getIdList());
    }

    @Override
    public Result<BasePageResult<Goods>> findByWays(GoodsPageRequest goodsPageRequest) {
        return goodsBiz.findByWays(goodsPageRequest);
    }

    @Override
    public Result<BaseRollPageResult<Goods>> scroll(BaseRollPageRequest baseRollPageRequest) {
        return goodsBiz.scroll(baseRollPageRequest);
    }
}
