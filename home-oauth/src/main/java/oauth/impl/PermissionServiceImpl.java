package oauth.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import oauth.orm.entity.Permission;
import oauth.orm.mapper.PermissionMapper;
import oauth.api.PermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2021-09-29
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {
   @Resource
    PermissionMapper permissionMapper;
    @Override
    public List<Permission> findByRoleId(String RoleId) {
        QueryWrapper<Permission> queryWrapper=new QueryWrapper();
        queryWrapper.lambda().eq(Permission::getRid,RoleId);
        return permissionMapper.selectList(queryWrapper);
    }
}
