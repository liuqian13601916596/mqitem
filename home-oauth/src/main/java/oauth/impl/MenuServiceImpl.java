package oauth.impl;

import app.response.MenuResponse;
import oauth.api.MenuService;
import oauth.biz.MenuBiz;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class MenuServiceImpl implements MenuService {
    @Resource
    MenuBiz menuBiz;
    @Override
    public List<MenuResponse> getMenu() {
        return menuBiz.getMenu();
    }
}
