package oauth.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import oauth.orm.entity.Routes;
import oauth.api.ExcleService;
import oauth.util.AjaxResult;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/11/6.
 */
@Service
public class ExcleServiceImpl implements ExcleService {
    @Override
    public AjaxResult imports(){
        String fileName = "D:\\myitem\\mqitem\\routes.xlsx";
        File file = new File("D:\\myitem\\mqitem\\routes.xlsx");
        List<Routes> userList = new ArrayList<>();
        EasyExcel.read(fileName, Routes.class, new AnalysisEventListener() {


            @Override
            public void invoke(Object o, AnalysisContext analysisContext) {
                System.out.println("解析一条Student对象：" + JSON.toJSONString(o));
                userList.add((Routes) o);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("excel文件读取完毕！");
            }
        }).sheet().doRead();
        System.out.println("读取出来的数据"+userList);
        if(!CollectionUtils.isEmpty(userList)){
            // userService.saveBatch(userList);
        }
        return  AjaxResult.success("读取成功");
    }
}
