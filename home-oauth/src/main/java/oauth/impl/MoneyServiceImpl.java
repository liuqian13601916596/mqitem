package oauth.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import oauth.orm.entity.Money;
import oauth.orm.mapper.MoneyMapper;
import oauth.api.MoneyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@Service
public class MoneyServiceImpl extends ServiceImpl<MoneyMapper, Money> implements MoneyService {

}
