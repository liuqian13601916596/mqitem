package oauth.biz;

import app.contant.GoodsLockKey;
import app.request.BaseRollPageRequest;
import app.request.GoodsPageRequest;
import app.request.GoodsRequest;
import app.response.BasePageResult;
import app.response.BaseRollPageResult;
import app.utils.util.Result;
import app.utils.util.Sequence;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import oauth.orm.mapper.GoodsMapper;
import oauth.api.GoodsService;
import oauth.api.UserStockService;
import oauth.orm.entity.Goods;
import oauth.orm.entity.UserStock;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Description:商品服务
 * @Author: liujinsheng
 * @Date: 2022/1/10.
 */
@Component
@Slf4j
public class GoodsBiz  {
    /**
     * 分布式锁
     */
    @Resource
    RedissonClient  redisson;
    @Resource
    GoodsService goodsService;
    @Resource
    UserStockService userStockService;
    @Resource
    GoodsMapper goodsMapper;
    @Resource
    RedisTemplate<String, String> redisTemplate;
    //保持串处理，防止并发的时候，事务提交多次,库存放在mysql
    @Transactional(isolation = Isolation.SERIALIZABLE )
    public Result<Boolean> updateMysqlStock(GoodsRequest goodsRequest){
    //用户id来保证唯一的锁
    String lockKey= GoodsLockKey.getGoodsLockKey(goodsRequest.getGoodsId());
    RLock rLock= redisson.getLock(lockKey);
        // 10s内可以自动释放锁
        rLock.lock(10,TimeUnit.SECONDS);
        Boolean flag=false;
        //先判断用户是否已经秒杀过
        QueryWrapper<UserStock> queryWrapperUs=new QueryWrapper();
        queryWrapperUs.lambda().eq(UserStock::getUserId,goodsRequest.getUserId());
        UserStock userStockUS= userStockService.getOne(queryWrapperUs);
        if(userStockUS!=null){
            return Result.ofFailMsg("您已经参与抢购了");
        }

    try {

        // 业务
        // 查询改商品的库存
        // 判断商品是否存在
           QueryWrapper<Goods> queryWrapper=new QueryWrapper();
           queryWrapper.lambda().eq(Goods::getId,goodsRequest.getGoodsId());
        Goods goods = goodsService.getOne(queryWrapper);
        if (goods == null) {
          return Result.ofFailMsg("该商品不存在");
        }
        // 库存要大于0
        Integer stock = goods.getStock();
        System.out.println("当前库存" + stock);
        if (stock <= 0) {
          return Result.ofFailMsg("库存不足");
        } else {
          // 修改商品的库存,保证原子性
          boolean blnStock = goodsMapper.updateStock(goodsRequest.getGoodsId(),stock );
          if (blnStock) {
            flag = true;
            UserStock userStock = new UserStock();
            Sequence sequence = new Sequence();
            String id = String.valueOf(sequence.nextId());
            userStock.setId(id);
            userStock.setUserId(goodsRequest.getUserId());
            userStock.setGoodsId(goodsRequest.getGoodsId());
            List<UserStock> list = new ArrayList();
            list.add(userStock);
            userStockService.saveBatch(list);
          }
          // 模拟如果出现错误
          //               int i=10;
          //               i=i/0;

        }

       }catch (Exception exception){
           log.error("商品库异常");
           //手动回滚事务，如果操作异常，手动回滚事务
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

     } finally{
        //最终释放锁
        rLock.unlock();
    }
    return  Result.ofSuccess(flag);
    }

    //库存放在redis
    @Transactional
    public Result<Boolean> updateRedisStock(GoodsRequest goodsRequest){
        //用户id来保证唯一的锁
        String lockKey= GoodsLockKey.getGoodsLockKey(goodsRequest.getGoodsId());
        RLock rLock= redisson.getLock(lockKey);

        String stockKey="mm:goods:stock:"+goodsRequest.getGoodsId();
        // 10s内可以自动释放锁
        rLock.lock(10, TimeUnit.SECONDS);
        Boolean flag=false;
        //先判断用户是否已经秒杀过
        QueryWrapper<UserStock> queryWrapperUs=new QueryWrapper();
        queryWrapperUs.lambda().eq(UserStock::getUserId,goodsRequest.getUserId());
        UserStock userStockUS= userStockService.getOne(queryWrapperUs);
        if(userStockUS!=null){
            return Result.ofFailMsg("您已经参与抢购了");
        }

        try {

            // 业务
            // 查询改商品的库存
            // 判断商品是否存在
            Boolean iSHas= redisTemplate.hasKey(stockKey);
            if (!iSHas) {
                return Result.ofFailMsg("该商品不存在");
            }
            Integer stock=0;
            // 库存要大于0
            String goodsStock=    redisTemplate.opsForValue().get(stockKey);
            if(StringUtils.isBlank(goodsStock)){

            }else{
                stock = Integer.parseInt(goodsStock);
            }
            System.out.println("当前库存" + stock);
            if (stock <= 0) {
                return Result.ofFailMsg("库存不足");
            } else {
                stock=stock-1;
                redisTemplate.opsForValue().set(stockKey,JSON.toJSONString(stock));

                    flag = true;
                    UserStock userStock = new UserStock();
                    Sequence sequence = new Sequence();
                    String id = String.valueOf(sequence.nextId());
                    userStock.setId(id);
                    userStock.setUserId(goodsRequest.getUserId());
                    userStock.setGoodsId(goodsRequest.getGoodsId());
                    List<UserStock> list = new ArrayList();
                    list.add(userStock);
                    userStockService.saveBatch(list);

                // 模拟如果出现错误,加上事务就会回滚
                //               int i=10;
                //               i=i/0;

            }

        }catch (Exception exception){
            log.error("商品库异常");
            //手动回滚事务，如果操作异常，手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

        } finally{
            //最终释放锁
            rLock.unlock();
        }
        return  Result.ofSuccess(flag);
    }

    public Map<String,Goods > getGoodsByidList(List<String> idList){
        Map<String,Goods > goodsMap=new HashMap<>();
      //先对id数组去重
        idList=idList.stream().distinct().collect(Collectors.toList());
    System.out.println("去重后的"+idList);
        //批量查询
        QueryWrapper<Goods> queryWrapper=new QueryWrapper();
        queryWrapper.lambda().in(Goods::getId,idList);
         List<Goods>  goodsList=  goodsService.list(queryWrapper);
         if(!CollectionUtils.isEmpty(goodsList)){
             goodsMap=  goodsList.stream().collect(Collectors.toMap(o -> o.getId().toString(),item ->item));

         }
         return  goodsMap;

    }
    public Result<Map<String,Goods >> getGoods(List<String> idList){
       return Result.ofSuccess(getGoodsByidList(idList)) ;
    }

    /**
     * 分页查询
     * @param goodsPageRequest
     * @return
     */
    public Result<BasePageResult<Goods>> findByWays(GoodsPageRequest goodsPageRequest){
        IPage<Goods > page=new Page<>(goodsPageRequest.getPage(),goodsPageRequest.getSize());

          List<Goods> goodsList= goodsService.page(page).getRecords();
        BasePageResult<Goods> basePageResult=new BasePageResult<>();
        basePageResult.setRecords(goodsList);
        basePageResult.setPage(goodsPageRequest.getPage());
        basePageResult.setSize(goodsPageRequest.getSize());
        basePageResult.setTotal(page.getTotal());
          return Result.ofSuccess(basePageResult);

    }

    /**
     * 滚动查询
     * @return
     */
    public Result<BaseRollPageResult<Goods>> scroll(BaseRollPageRequest baseRollPageRequest){
        IPage<Goods > page=new Page<>(baseRollPageRequest.getNext(),baseRollPageRequest.getSize());

        List<Goods> goodsList= goodsService.page(page).getRecords();
        BaseRollPageResult<Goods> baseRollPageResult=new BaseRollPageResult<>();
           if(!CollectionUtils.isEmpty(goodsList)){
               if(goodsList.size()<baseRollPageRequest.getSize()){
                   System.out.println("数量"+goodsList.size());
                   System.out.println("没有下一页");
                   //如果查出来的数量小每页的条数。说明没有下一页
                   baseRollPageResult.setHasNext(false);
               }else{
                   System.out.println("有下一页");
                   Long indexPage=baseRollPageRequest.getNext();
                   //每页加一
                   indexPage+=1;
                   baseRollPageResult.setHasNext(true);
                   baseRollPageResult.setNext(indexPage);
               }
               baseRollPageResult.setRecords(goodsList);
               baseRollPageResult.setSize(goodsList.size());

           }
        System.out.println(baseRollPageResult);
       return  Result.ofSuccess(baseRollPageResult);
    }


}
