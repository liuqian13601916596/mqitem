package oauth.biz;

import app.response.MenuResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class MenuBiz {
    /**
     * 获取菜单的信息
     * @return
     */
    public List<MenuResponse> getMenu() {
        //模拟从数据库查出来的数据
        List<MenuResponse> menuResponseList= Arrays.asList(
                new MenuResponse(1L,"根节点",0L),
                new MenuResponse(2L,"节点1",1L),
                new MenuResponse(3L,"节点2",2L),
                new MenuResponse(4L,"节点3",3L),
                new MenuResponse(5L,"节点4",4L),
                new MenuResponse(6L,"节点5",5L),
                new MenuResponse(7L,"节点6",6L),
                new MenuResponse(8L,"节点7",7L),
                new MenuResponse(9L,"节点8",8L),
                new MenuResponse(10L,"节点9",9L),
                new MenuResponse(11L,"节点10",10L),
                new MenuResponse(12L,"节点11",11L),
                new MenuResponse(13L,"节点12",12L),
                new MenuResponse(14L,"节点13",13L),
                new MenuResponse(15L,"节点14",14L),
                new MenuResponse(16L,"节点15",15L),
                new MenuResponse(17L,"节点16",16L),
                new MenuResponse(18L,"节点17",17L)

        );
        //获取父节点
        List<MenuResponse> collect=menuResponseList.stream().filter(menuResponse -> menuResponse.getParentId()==0).map((m) ->{
            m.setChildList(getChildrens(m,menuResponseList));
            return m;
        }).collect(Collectors.toList());

       return collect;
    }

    /***
     * 递归查询子节点
     * @param root 根节点
     * @param all 所有节点
     * @return
     */
    public List<MenuResponse> getChildrens(MenuResponse root,List<MenuResponse> all){
        List<MenuResponse> children=all.stream().filter(m->{
            return Objects.equals(m.getParentId(),root.getId());
        }).map((m) ->{
            m.setChildList(getChildrens(m,all));
            return m;
        }).collect(Collectors.toList());
        return  children;

    }
}
