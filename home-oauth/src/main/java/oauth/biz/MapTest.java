package oauth.biz;

import oauth.orm.entity.Goods;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2022/1/12.
 */
public class MapTest {
  public static void main(String[] args) {
      Map<String, Goods> map = new HashMap();
      Goods goods0=new Goods();
      BigDecimal bigDecimal=new BigDecimal("10");

      goods0.setPrice(bigDecimal);
      goods0.setStock(10);
      goods0.setId(100L);
      map.put("10",goods0);
      Goods goods1=new Goods();


      goods1.setPrice(bigDecimal);
      goods1.setStock(10);
      map.put("10",goods0);
      Goods goods2=new Goods();
      BigDecimal bigDecimal2=new BigDecimal("100");

      goods2.setPrice(bigDecimal2);
      goods2.setStock(10);
      map.put("100",goods2);
      Goods goods3=new Goods();
      BigDecimal bigDecimal3=new BigDecimal("10");

      goods3.setPrice(bigDecimal3);
      goods3.setStock(10);
      map.put("10",goods3);

      Goods goods=map.get("10");
      System.out.println(goods);

    //
  }
}
