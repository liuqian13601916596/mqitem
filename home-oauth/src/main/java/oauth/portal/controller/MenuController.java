package oauth.portal.controller;

import app.response.MenuResponse;
import oauth.api.MenuService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class MenuController {
    @Resource
    MenuService menuService;
    @PostMapping("/getMenu")
    public List<MenuResponse> getMenu() {
        return menuService.getMenu();
    }

}
