package oauth.portal.controller;


import app.request.BaseRollPageRequest;
import app.request.GoodsListRequest;
import app.request.GoodsPageRequest;
import app.request.GoodsRequest;
import app.response.BasePageResult;
import app.response.BaseRollPageResult;
import app.utils.util.Result;
import oauth.api.GoodsService;
import oauth.orm.entity.Goods;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@RestController
@RequestMapping("/pay/goods")
public class GoodsController {
    @Resource
    GoodsService goodsService;
    @GetMapping("/updateMyqlStock")
    public Result<Boolean> updateMyqlStock( GoodsRequest goodsRequest){
        return goodsService.updateMysqlStock(goodsRequest);
    }
    @GetMapping("/updateRedisStock")
    public Result<Boolean> updateRedisStock(GoodsRequest goodsRequest) {
        return goodsService.updateRedisStock(goodsRequest);
    }
    @GetMapping("/initStock")
    public void initStock() {
         goodsService.initStock();
    }
    @PostMapping("/getGoods")
    public Result<Map<String, Goods>> getGoods(@RequestBody GoodsListRequest goodsListRequest){
        return goodsService.getGoods(goodsListRequest);
    }
    @GetMapping("/findByWays")
    public Result<BasePageResult<Goods>> findByWays(GoodsPageRequest goodsPageRequest) {

        return goodsService.findByWays(goodsPageRequest);
    }
    @GetMapping("/scroll")
    public Result<BaseRollPageResult<Goods>> scroll(BaseRollPageRequest baseRollPageRequest) {
        return goodsService.scroll(baseRollPageRequest);
    }
}
