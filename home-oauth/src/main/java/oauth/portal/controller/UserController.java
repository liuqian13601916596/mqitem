package oauth.portal.controller;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import oauth.api.UserService;
import oauth.orm.entity.Routes;
import oauth.orm.entity.User;
import oauth.util.AjaxResult;
import oauth.vo.UserRequest;
import oauth.vo.UserResponse;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ljs
 * @since 2021-09-10
 */
@RestController
@RequestMapping("/pay/user")
public class UserController {
    @Resource
    UserService userService;
    @GetMapping("/test")
     public List test(){
    System.out.println(userService.list());
        return   userService.list();
     }
     @GetMapping("/phone")

    public String size(String phone){
        return phone;
    }

    /**
     * 对list数据源将其里面的数据导入到excel表单（EasyExcel）
     *
     * 导出数据集合
     *   工作表的名称
     * @return 结果
     */
    @GetMapping("/export")
    public void downloadFailedUsingJson(HttpServletResponse response) throws IOException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        try {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("用户", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            // 这里需要设置不关闭流
          List<User>  userList= userService.list();
            EasyExcel.write(response.getOutputStream(), User.class).autoCloseStream(Boolean.FALSE).sheet("模板")
                    .doWrite(userList);
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap<String, String>();
            map.put("status", "failure");
            map.put("message", "下载文件失败" + e.getMessage());
            response.getWriter().println(JSON.toJSONString(map));
        }
    }
    /**
     *  导入
     */

    @GetMapping("/import")
    public AjaxResult  imports(){
    String fileName = "D:\\myitem\\mqitem\\routes.xlsx";
    File file = new File("D:\\myitem\\mqitem\\routes.xlsx");
        List<Routes> userList = new ArrayList<>();
          EasyExcel.read(fileName, Routes.class, new AnalysisEventListener() {


              @Override
              public void invoke(Object o, AnalysisContext analysisContext) {
                  System.out.println("解析一条Student对象：" + JSON.toJSONString(o));
              userList.add((Routes) o);
              }

              @Override
              public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                  System.out.println("excel文件读取完毕！");
              }
          }).sheet().doRead();
    System.out.println("读取出来的数据"+userList);
    if(!CollectionUtils.isEmpty(userList)){
       // userService.saveBatch(userList);
    }
     return  AjaxResult.success("读取成功");
    }

    @PostMapping("/getUser")
    public UserResponse getUser(@RequestBody UserRequest userRequest ){
     return    userService.findUser(userRequest);
    }

}

