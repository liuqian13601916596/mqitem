package app.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/9/28.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigApplication {
  public static void main(String[] args) {
    SpringApplication.run(ConfigApplication.class,args);
  }
}
