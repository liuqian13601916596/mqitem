package comsumer.feign;

import comsumer.feign.erro.TestErro;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * @Description:用feign来调服务，通过服务名来调用
 * @Author: liujinsheng
 * @Date: 2021/9/28.
 */
@FeignClient(value = "nacos-payment-provider",fallback = TestErro.class )
public interface TestService {
    @GetMapping("/provider/test")
    public String test();
}
