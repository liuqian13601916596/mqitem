package comsumer.feign.erro;

import comsumer.feign.TestService;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/9/28.
 */
@Component
public class TestErro implements TestService {
    @Override
    public String test() {
        return "接口调用出错";
    }
}
