package comsumer.controller;

import comsumer.feign.TestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description:
 * @Author: liujinsheng
 * @Date: 2021/9/28.
 */
@RestController
public class GetTestController {
    @Resource
    TestService testService;
    @GetMapping("/getTest")
    public String getTest(){
        return testService.test();
    }

}
