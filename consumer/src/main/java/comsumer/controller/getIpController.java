package comsumer.controller;

import app.utils.util.IPAddrUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class getIpController {
    @ResponseBody
    @GetMapping("/getIp")
    public String getIp(HttpServletRequest request){
       return IPAddrUtils.getIpaddr(request);


    }
}
